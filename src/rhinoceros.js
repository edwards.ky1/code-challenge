const uuidv4 = require('uuid/v4');
let rhinoceroses = require('./data');

exports.getAll = () => {
  return rhinoceroses;
};

exports.getEndangered = () => {
	let counts = {};
	rhinoceroses.forEach((rhino) => {
	  if (counts[rhino.species]) {
	  	counts[rhino.species].count += 1
	  	counts[rhino.species].rhinos.push(rhino)
	  } else {
	    counts[rhino.species] = { "count": 1, "rhinos": [rhino] }
	  }
	})

	endangered = [];
	Object.keys(counts).forEach((species) => {
		if (counts[species].count <= 2) {
			endangered = endangered.concat(counts[species].rhinos)
		}
	})
  return endangered;
};

exports.get = (id) => {
  return rhinoceroses.find((r) => {return r.id == id});
};

exports.newRhinoceros = data => {
  const newRhino = {
    id: uuidv4(),
    name: data.name,
    species: data.species
  };
  rhinoceroses.push(newRhino);
  return newRhino;
};
