const Joi = require('@hapi/joi');

module.exports = Joi.object().keys({
    name: Joi.string().alphanum().min(1).max(20).required(),
    species: Joi.string().valid('white_rhinoceros', 'black_rhinoceros', 
    	'indian_rhinoceros', 'javan_rhinoceros', 'sumatran_rhinoceros')
    .required()
})