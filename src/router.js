const Router = require('koa-router'),
  router = new Router(),
  model = require('./rhinoceros'),
  scheme = require('./scheme');

router.get('/rhinoceros', (ctx, next) => {
  let rhinoceroses = model.getAll();
  if (ctx.request.query.species) {
  	rhinoceroses = rhinoceroses.filter((r) => {return r.species == ctx.request.query.species});
  }
  if (ctx.request.query.name) {
  	rhinoceroses = rhinoceroses.filter((r) => {return r.name == ctx.request.query.name});
  }
  ctx.response.body = { rhinoceroses };
});

router.get('/rhinoceros/endangered', (ctx, next) => {
  const endangered = model.getEndangered();
  ctx.response.body = { endangered };
});

router.param('rhinoceros', (id, ctx, next) => {
    ctx.rhinoceros = model.get(id)
    if (!ctx.rhinoceros) return ctx.status = 404;
    return next();
  })
  .get('/rhinoceros/:rhinoceros', ctx => {
    ctx.response.body = ctx.rhinoceros;
  })

router.post('/rhinoceros', (ctx, next) => {
  const { error, value } = scheme.validate(ctx.request.body);
  if (error) {
  	ctx.response.body = error.details
  	return ctx.status = 400
  } else {
    ctx.response.body = model.newRhinoceros(ctx.request.body);
  }
});

module.exports = router;
